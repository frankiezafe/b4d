# B4D

![](https://polymorph.cool/wp-content/uploads/2018/10/blender4graphicdesigners.png)

blender for graphic designers

by [frankiezafe](http://polymorph.cool)

This repository contains notes & files related to the topic: how to propose blender as a tool for graphic designers, therefore focused on text manipulation and SVG import & export.

An important addon: [Freestyle SVG Exporter](https://docs.blender.org/manual/en/latest/render/freestyle/export_svg.html).

## gallery

![](https://polymorph.cool/wp-content/uploads/2018/11/20181126_ludi_OSPschool_erg.png)
screenshot by @Ludi@framapiaf.org

## example.blend

![](https://polymorph.cool/wp-content/uploads/2018/11/screenshot_cycle_rendering.png)
![](https://polymorph.cool/wp-content/uploads/2018/11/screenshot_python_splines.png)
![](https://polymorph.cool/wp-content/uploads/2018/11/screenshot_svg_export.png)
![](https://polymorph.cool/wp-content/uploads/2018/11/screenshot_b4d_texture_mapping.png)


 have fun and be cool :)
 
 ___________________________________( ^3^)_____________________________________
